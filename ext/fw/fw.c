#include <ruby.h>

static VALUE fw(VALUE arr1, VALUE path) {
  int i, j, k;
  float ij, ik, kj, new_distance;
  int size;

  size = RARRAY_LEN(arr1);

  for(k = 0; k < size; k++) {
    for (i = 0; i < size; i++){
      for (j = 0; j < size; j++) {
        ij = NUM2DBL(rb_ary_entry(rb_ary_entry(arr1, i), j));
        ik = NUM2DBL(rb_ary_entry(rb_ary_entry(arr1, i), k));
        kj = NUM2DBL(rb_ary_entry(rb_ary_entry(arr1, k), j));
        new_distance = ik + kj;
        if(ij > new_distance) {
          rb_ary_store(rb_ary_entry(arr1, i), j, rb_float_new(new_distance));
          rb_ary_store(rb_ary_entry(path, i), j, rb_ary_new3(1, rb_float_new(k)));
        }
        if((ij == new_distance) && !RTEST(rb_ary_includes(rb_ary_entry(rb_ary_entry(path, i), j), rb_float_new(k)))&&  !(i == k || j == k)) {
          rb_ary_push(rb_ary_entry(rb_ary_entry(path, i), j), rb_float_new(k));
        }
      }
    }
  }
  return arr1;
}

void Init_fw() {
  rb_define_method(rb_cArray, "fw!", fw, 1);
}
