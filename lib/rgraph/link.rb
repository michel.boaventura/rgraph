class Link < Hash
  def initialize(attributes)
    self[:type] = 'undirected'
    self[:weight] = 1

    attributes.each_pair do |key, value|
      self[key] = value
    end

    raise Exception.new("source cant be nil") unless self[:source]
    raise Exception.new("target cant be nil") unless self[:target]
    raise Exception.new("source must be of type Node") unless self[:source].is_a? Node
    raise Exception.new("target must be of type Node") unless self[:target].is_a? Node

    self[:source].neighbours << self[:target]
    self[:target].neighbours << self[:source] unless self[:type] == 'directed'
  end

  def source
    self[:source]
  end

  def target
    self[:target]
  end

  def type
    self[:type]
  end
end
