class Node
  attr_accessor :neighbours

  def initialize(attributes)
    @attributes = attributes
    @neighbours = []
  end

  def degree
    @neighbours.size
  end

  def has_neighbour?(node)
    @neighbours.include?(node)
  end

  def [](attribute)
    @attributes[attribute]
  end
end
