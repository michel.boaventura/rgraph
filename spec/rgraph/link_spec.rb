require 'spec_helper'

describe Link do
  describe "creates a link without a weight" do
    subject { Link.new(source: Node.new(id: 1), target: Node.new(id: 2), years: [2011, 2012]) }

    it "returns its attributes" do
      expect(subject.source).to be_kind_of(Node)
      expect(subject.target).to be_kind_of(Node)
      expect(subject[:weight]).to eq(1)
      expect(subject[:years]).to eq([2011,2012])
    end

    it "checks the creation of neighbours" do
      expect(subject.source.neighbours).to eq([subject.target])
      expect(subject.target.neighbours).to eq([subject.source])
    end

    it "checks link's direction" do
      link = Link.new(source: Node.new(id: 1), target: Node.new(id: 2), type: 'directed')
      expect(link.source.neighbours.count).to eq(1)
      expect(link.target.neighbours.count).to eq(0)
    end

  end

  it "creates a link passing a weight" do
    link = Link.new(source: Node.new(id: 00001), target: Node.new(id: 00002), weight: 4, years: [2011, 2012])
    expect(link[:weight]).to eq(4)
  end

  it "doesn't create a link without source" do
    expect { Link.new(target: Node.new(id: 00001)) }.to raise_exception("source cant be nil")
  end

  it "doesn't create a link without target" do
    expect { Link.new(source: Node.new(id: 00001)) }.to raise_exception("target cant be nil")
  end

  it "doesn't create a link with wrong source type" do
    expect { Link.new(source: "Lucas", target: Node.new(id: 1)) }.to raise_exception("source must be of type Node")
  end

  it "doesn't create a link with wrong target type" do
    expect { Link.new(source: Node.new(id: 1), target: "Lucas") }.to raise_exception("target must be of type Node")
  end

  it "sets default type as 'undirected'" do
    expect(Link.new(source: Node.new(id: 1), target: Node.new(id: 2)).type).to eq('undirected')
  end

end
