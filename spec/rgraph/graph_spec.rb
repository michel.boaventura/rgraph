require 'spec_helper'

describe Graph do
  describe "read .csv" do
    subject { Graph.new('spec/fixtures/three_links.csv') }

    it "creates three nodes" do
      expect(subject.nodes.size).to eq(3)
    end

    it "creates all neighbours" do
      nodes = subject.nodes.values[1..-1]
      expect(subject.nodes.values.first.neighbours).to eq(nodes)
    end

    it "creates three links" do
      expect(subject.links.size).to eq(3)
    end

    it "gets all nodes one by one" do
      nodes = {}
      subject.each_node do |node|
        nodes[node[:id]] = node
      end
      expect(subject.nodes).to eq(nodes)
    end

    it "gets all links one by one" do
      links = []
      subject.each_link do |link|
        links << link
      end
      expect(subject.links).to eq(links)
    end

    it "creates 445 links" do
      expect(Graph.new('spec/fixtures/2005.csv').nodes.size).to eq(445)
    end

    it "creates a graph with string as input" do
      graph = Graph.new_from_string("source,target\n1,2")
      expect(graph.nodes.count).to eq(2)
      expect(graph.links.count).to eq(1)
    end

    it "understands directed graph" do
      graph = Graph.new_from_string("source,target,type\n1,2,directed")
      expect(graph.nodes.values.first.neighbours.count).to eq(1)
      expect(graph.nodes.values.last.neighbours.count).to eq(0)
      expect(graph.links.count).to eq(1)
    end
  end

  describe "Degree" do
    subject { Graph.new('spec/fixtures/three_links.csv') }

    it "sets default infinity to 100_000" do
      expect(subject.infinity).to eq(100_000)
    end

    it "all nodes degree" do
      expect(subject.degrees).to eq([2,2,2])
    end
    it "average degree" do
      expect(subject.average_degree).to eq(2)
    end
    it "cumulative degree" do
      expect(subject.cumulative_degree).to eq([1.0, 1.0])
    end
  end

  describe "Distance" do
    subject { Graph.new('spec/fixtures/small_graph.csv') }

    it "calculates the initial distance matrix" do
      k = subject.infinity
      expect(subject.idistance).to eq(
        [[0,1,1,k,k,k],
         [1,0,1,1,k,k],
         [1,1,0,k,1,k],
         [k,1,k,0,1,k],
         [k,k,1,1,0,1],
         [k,k,k,k,1,0]])
    end

    it "calculates distances" do
      expect(subject.distances).to eq(
        [[0,1,1,2,2,3],
         [1,0,1,1,2,3],
         [1,1,0,2,1,2],
         [2,1,2,0,1,2],
         [2,2,1,1,0,1],
         [3,3,2,2,1,0]])
    end

    it "calculates the mean distance" do
        mdistance = subject.distances.flatten
        mdistance = mdistance.inject(:+) / mdistance.count.to_f
        expect(subject.mdistance).to eq(mdistance)
    end

    it "calculates the mean distance ignoring 'infinity'" do
      graph = Graph.new('spec/fixtures/two_links_with_hole.csv')
      expect(graph.mdistance).to eq(10.0/13.0)
    end

    it "understands holes on the graph" do
      graph = Graph.new('spec/fixtures/two_links_with_hole.csv')
      k = graph.infinity
      expect(graph.idistance).to eq(
        [[0,1,k,k,k],
         [1,0,1,k,k],
         [k,1,0,k,k],
         [k,k,k,0,1],
         [k,k,k,1,0]])
      expect(graph.distances).to eq(
        [[0,1,2,k,k],
         [1,0,1,k,k],
         [2,1,0,k,k],
         [k,k,k,0,1],
         [k,k,k,1,0]])
    end
  end
  describe "Paths' metrics" do
    subject { Graph.new('spec/fixtures/small_graph.csv') }

    it "calculates a simple shortest path" do
      g = Graph.new_from_string("source,target\n1,2")
      expect(g.shortest_paths).to eq([[0, 1]])
    end

    it "doesn't return empty paths" do
      g = Graph.new_from_string("source,target\n1,2\n3,4")
      expect(g.shortest_paths).to eq([[0,1],[2,3]])
    end

    it "calculates the shortest paths" do
      expect(subject.shortest_paths.sort).to eq(
        [[0, 1],
         [0, 1, 3],
         [0, 2],
         [0, 2, 4],
         [0, 2, 4, 5],
         [1, 2],
         [1, 2, 4],
         [1, 2, 4, 5],
         [1, 3],
         [2, 1, 3],
         [2, 4],
         [2, 4, 5],
         [3, 4],
         [3, 4, 5],
         [4, 5]].sort)
    end

    it "returns multiples short paths when asked" do
      g = Graph.new_from_string("source,target\n1,2\n2,3\n1,4\n4,3")
      expect(g.shortest_paths.count).to eq(6)
      expect(g.shortest_paths(multiples: true).count).to eq(8)
    end

    it "calculates the betweenness of the giant component" do
      g = Graph.new_from_string("source,target\n1,2\n2,3\n3,4\n5,6")
      expect(g.between(0, giant: true)).to eq(0 / 6.0)
      expect(g.between(1, giant: true)).to eq(2 / 6.0)
      expect(g.between(2, giant: true)).to eq(2 / 6.0)
      expect(g.between(3, giant: true)).to eq(0 / 6.0)
      expect(g.between(4, giant: true)).to eq(0.0)
      expect(g.between(5, giant: true)).to eq(0.0)
      expect(g.betweenness(giant: true)).to eq([0, 1 / 3.0, 1 / 3.0, 0, 0, 0])
    end

    #FIXME - should be 17!!!
    it "calculates the betweenness of a single node" do
      expect(subject.between(0)).to eq(0 / 17.0)
      expect(subject.between(1)).to eq(2 / 17.0)
      expect(subject.between(2)).to eq(4 / 17.0)
      expect(subject.between(3)).to eq(1 / 17.0)
      expect(subject.between(4)).to eq(5 / 17.0)
      expect(subject.between(5)).to eq(0 / 17.0)
    end

    it "calculates all betweenness" do
      expect(subject.betweenness).to eq(
        [0 / 17.0,
         2 / 17.0,
         4 / 17.0,
         1 / 17.0,
         5 / 17.0,
         0 / 17.0])
    end

    it "calculates all betweenness normalized" do
      bts = subject.betweenness
      min = bts.min
      max = bts.max

      bts.map!{|bt| (bt - min) / (max - min)}

      expect(subject.betweenness(normalized: true)).to eq(bts)
    end

    it "works with holes" do
      g = Graph.new_from_string("source,target\n1,2\n3,4")
      expect(g.betweenness.inject(:+)).to eq(0)
    end

    it "calculates cumulative betweenness" do
      bts = subject.betweenness(normalized: true)

      bts = bts.uniq.sort.map{ |bt1| [bts.select{|bt2| bt2 >= bt1}.count, bt1] }

      expect(subject.betweenness(cumulative: true)).to eq(bts)
    end

    it "calculates diameter" do
      expect(subject.diameter).to eq(3)
    end

    it "calculates farness" do
      expect(subject.farness).to eq([9,8,7,8,7,11])
    end

    it "calculates closeness" do
      expect(subject.closeness).to eq([1/9.0,1/8.0,1/7.0,1/8.0,1/7.0,1/11.0])
    end

    it "returns the graph's components" do
      g = Graph.new_from_string("source,target\n1,2\n3,4")
      expect(g.components).to eq([g.nodes.values[0..1], g.nodes.values[2..3]])
    end

    it "sets the giant component" do
      g = Graph.new_from_string("source,target\n1,2\n2,3\n4,5")
      expect(g.giant_component.size).to eq(3)
    end

    it "calculates clustering of a single node" do
      nodes = subject.nodes.values

      expect(subject.single_clustering(nodes[0])).to eq(1.0/1.0)
      expect(subject.single_clustering(nodes[1])).to eq(1.0/3.0)
      expect(subject.single_clustering(nodes[2])).to eq(1.0/3.0)
      expect(subject.single_clustering(nodes[3])).to eq(0.0/1.0)
      expect(subject.single_clustering(nodes[4])).to eq(0.0/3.0)
      expect(subject.single_clustering(nodes[5])).to eq(0.0/1.0)
    end

    it "calculates clustering of all nodes" do
      expect(subject.clustering).to eq((1.0/1.0 + 2.0/3.0) / 6.0)
    end
  end
  describe "PageRank" do
    it "calculates with two nodes connected" do
      pr = Graph.new_from_string("source,target,type\n1,2,directed").page_rank
      expect(pr.first).to be < pr.last
    end

    it "calculates with three nodes within a line" do
      pr = Graph.new_from_string("source,target,type\n1,2,directed\n2,3,directed").page_rank
      expect(pr[0]).to be < pr[1]
      expect(pr[1]).to be < pr[2]
    end

    it "calculates with two nodes pointing to the same" do
      pr = Graph.new_from_string("source,target,type\n1,2,directed\n3,2,directed").page_rank
      expect(pr[0]).to be < pr[1]
      expect(pr[2]).to be < pr[1]
      expect(pr[0]).to eq(pr[2])
    end

    it "calculates with a cycle" do
      pr = Graph.new_from_string("source,target,type\n1,2,directed\n2,3,directed\n3,1,directed").page_rank
      expect(pr[0]).to eq(pr[1])
      expect(pr[1]).to eq(pr[2])
      expect(pr.inject(:+)).to eq(1)
    end

    it "calculates with two subgraphs" do
      pr = Graph.new_from_string("source,target,type\n1,2,directed\n3,4,directed\n3,1,directed").page_rank
      expect(pr[0]).to be < pr[1]
      expect(pr[2]).to be < pr[3]
    end
  end
end
