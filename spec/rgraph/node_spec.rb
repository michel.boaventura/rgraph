require 'spec_helper'

describe Node do
  subject { Node.new(name: "Lucas", id: '1', age: 20) }

  context "#new" do
    it "returns its attributes" do
      expect(subject[:name]).to eq("Lucas")
      expect(subject[:id]).to eq('1')
      expect(subject[:age]).to eq(20)
    end
    it "knows it degree" do
      expect(subject.degree).to eq(0)
    end
  end

  context "#has_neighbour" do
    it "recognizes its neighbour" do
      n1 = Node.new(id: 1)
      n2 = Node.new(id: 1)
      n1.neighbours << n2
      expect(n1.has_neighbour?(n2)).to be true
    end
  end
end
