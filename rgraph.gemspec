lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rgraph/version'

Gem::Specification.new do |spec|
  spec.name          = "rgraph"
  spec.version       = Rgraph::VERSION
  spec.authors       = ["Michel Boaventura"]
  spec.email         = ["michel.boaventura@gmail.com"]
  spec.description   = "Ruby's Graph Library"
  spec.summary       = "A Ruby's Graph Library"
  spec.homepage      = "http://github.com/michelboaventura/rgraph"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.test_files    = spec.files.grep(%r{^spec/})
  spec.require_paths = ["lib"]

  spec.extensions = %w[ext/fw/extconf.rb]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 12.0"
  spec.add_development_dependency "rake-compiler", "~> 1.0"
  spec.add_development_dependency "rspec", "~> 3.6"
  spec.add_development_dependency "guard-rspec", "~> 4.7"
end
